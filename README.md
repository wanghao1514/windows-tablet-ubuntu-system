# Windows平板电脑安装Ubuntu系统


#### 介绍
Windows平板安装Ubuntu系统，以CPU是Z3735F为例。


#### 使用工具
1. 系统镜像 ubuntu-20.04-desktop-amd64.iso
2. U盘引导制作工具 rufus-3.14.exe
3. Windows平板电脑
4. U盘（8G）
5. 32位引导启动文件 bootia32.efi

#### 参考网站
> https://www.linuxidc.com/Linux/2016-09/135501.htm

> https://rufus.ie/zh/

> https://blog.csdn.net/eloudy/article/details/79841531

> https://github.com/hirotakaster/baytail-bootia32.efi

> https://next.itellyou.cn/


#### 安装教程

1. 下载需要的工具
- 系统镜像 ubuntu-20.04-desktop-amd64.iso
> 浏览地址：https://next.itellyou.cn/Original/Index#cbp=Product?ID=deb4715d-5e52-ea11-bd34-b025aa28351d

> 下载地址种子：magnet:?xt=urn:btih:9FC20B9E98EA98B4A35E6223041A5EF94EA27809&dn=ubuntu-20.04-desktop-amd64.iso&xl=2715254784

- U盘引导制作工具 rufus-3.14.exe（git中已上传）

> 浏览地址：https://rufus.ie/zh/

> 下载地址：https://github-releases.githubusercontent.com/2810292/0ad62f00-a9d1-11eb-9f83-315d019ee57b?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20210507%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210507T134215Z&X-Amz-Expires=300&X-Amz-Signature=4d508d290074c631eafb1b84914519bffb2002d30bb2cd9dc7afeb391586f90c&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2810292&response-content-disposition=attachment%3B%20filename%3Drufus-3.14.exe&response-content-type=application%2Foctet-stream

- 32位引导启动文件 bootia32.efi（git中已上传）
> 浏览地址：https://github.com/hirotakaster/baytail-bootia32.efi


2.  制作U盘启动
- 以管理员的身份打开“U盘引导制作工具 rufus-3.14.exe”。
- 做如下配置。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/212949_d78fe2ee_2064914.png "屏幕截图.png")
- 点击开始按钮
- 当状态栏已全部完成时，即制作完成。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/213224_983e320a_2064914.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/213259_890a08b5_2064914.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/213320_dbade785_2064914.png "屏幕截图.png")
-  **修改UEFI引导文件，将bootia32.efi拷贝到U盘的/EFI/BOOT/文件夹下。** 没有此步骤32位CPU会启动不了系统引导！！！

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/215148_f48e812a_2064914.png "屏幕截图.png")
> 
- U盘启动制作完成。


3.  开始安装系统
> 选择ubuntu， 其他按提示操作就行，这里就不做说明了。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/220425_7ff819b2_2064914.png "屏幕截图.png")

